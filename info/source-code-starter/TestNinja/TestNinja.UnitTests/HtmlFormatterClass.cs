﻿

using NUnit.Framework;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests
{
    [TestFixture]
    public class HtmlFormatterClass
    {
        [Test]
        [TestCase("<strong>abc</strong>")]
        [TestCase("<strong></strong>")]
        [TestCase("<strong><strong></strong></strong>")]
        public void HtmlFormatter_WhenCall_ShouldEncloseTheStringWithStrongElement(string html)
        {
            var Formatter = new HtmlFormatter();

            var result = Formatter.FormatAsBold(html);

            // Specific 
            Assert.That(result, Is.EqualTo("<strong>"+ html + "</strong>"));

            // More general
            Assert.That(result, Does.StartWith("<strong>"));
            Assert.That(result, Does.EndWith("</strong>"));
            Assert.That(result, Does.Contain(html));
        }
    }
}
