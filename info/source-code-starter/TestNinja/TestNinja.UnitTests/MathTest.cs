﻿using NUnit.Framework;
//using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests
{
    [TestFixture]
    public class MathTest
    {
        private Math _math;

        // Setup
        // TearDown

        [SetUp]
        public void SetUp()
        {
            // Arrange
            _math = new Math();
        }
        #region Before
        //[Test]
        //public void Add_WhenCall_ReturnSumOfArguments()
        //{
        //    // Act
        //    var result = _math.Add(1, 2);

        //    // Assert
        //    Assert.That(result, Is.EqualTo(3));
        //}

        //[Test]
        //public void Max_FirstArgumentIsGreater_ReturnTheFirstArgument()
        //{
        //    // Act
        //    var result = _math.Max(2, 1);

        //    // Assert
        //    Assert.That(result, Is.EqualTo(2));
        //}

        //[Test]
        //public void Max_SecondArgumentIsGreater_ReturnTheSecondArgument()
        //{
        //    // Act
        //    var result = _math.Max(3, 4);

        //    // Assert
        //    Assert.That(result, Is.EqualTo(4));
        //}

        //[Test]
        //public void Max_ArgumentIsGreater_ReturnTheSameArgument()
        //{
        //    // Act
        //    var result = _math.Max(8, 8);

        //    // Assert
        //    Assert.That(result, Is.EqualTo(8));
        //}
        #endregion

        [Test]
        //[Ignore("เพราะว่า ต้องการมัน")]
        public void Add_WhenCall_ReturnSumOfArguments()
        {
            // Act
            var result = _math.Add(1, 2);

            // Assert
            Assert.That(result, Is.EqualTo(3));
        }

        [Test]
        [TestCase(2, 1, 2)]
        [TestCase(3, 4, 4)]
        [TestCase(5, 5, 5)]
        public void Max_WhenCalled_ReturnGreaterArgument(int a, int b, int expecdResult)
        {
            // Act
            var result = _math.Max(a, b);

            // Assert
            Assert.That(result, Is.EqualTo(expecdResult));
        }

        [Test]
        public void GetOddNumbers_LimitGreaterThenZero()
        {
            // Act
            var result = _math.GetOddNumbers(5);

            // Assert
            Assert.That(result, Is.Not.Empty);
            Assert.That(result, Does.Contain(1));
            Assert.That(result, Does.Contain(3));
            Assert.That(result, Does.Contain(5));
            Assert.That(result, Is.EquivalentTo(new[] { 1, 3, 5 }));


        }
    }
}
