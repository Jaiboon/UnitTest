﻿using System;
using NUnit.Framework;
using TestNinja.Fundamentals;

namespace TestNinja.UnitTests
{
    [TestFixture]
    public class ReservationTests
    {
        [Test]
        public void CanBeCancelledBy_UserIsAdmin_RetureTrue()
        {
            // Arrange
            var rservation = new Reservation();

            // Act
            var result = rservation.CanBeCancelledBy(new User { IsAdmin = true });

            // Assert
            Assert.That(result, Is.True);
        }

        [Test]
        public void CanBeCancelledBy_SameUserCancellingTheResertion_RetureTrue()
        {
            // Arrange
            var user = new User();
            var rservation = new Reservation { MadeBy = user };

            // Act
            var result = rservation.CanBeCancelledBy(user);

            // Assert
            Assert.That(result, Is.True);

        }

        [Test]
        public void CanBeCancelledBy_AnotherUserCancellingResertion_RetureFalse()
        {
            // Arrange
            var rservation = new Reservation { MadeBy = new User() };

            // Act
            var result = rservation.CanBeCancelledBy(new User());

            // Assert
            Assert.That(result, Is.False);

        }
    }
}
